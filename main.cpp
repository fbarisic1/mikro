/*
 Tema: Brzinometar
 Autor: Filip Barisic, Matijas Bozic
*/ 

#include <avr/io.h>
#include "AVR VUB/avrvub.h"
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h> // Dodano za EEPROM
#include "lcd.h"

// Definicije za svjetla
#define SHORT_LIGHT_PIN PB1
#define LONG_LIGHT_PIN PB2

#define OPSEG_KOTACA 0.5 // Opseg kota�a u metrima
#define BROJ_MAGNETA 1 // Broj magneta na kota�u

volatile uint16_t brojacImpulsa = 0;
volatile float brzina = 0.0;
volatile float ukupnaUdaljenost = 0.0;

void inicijalizacija() {
    inicijalizacijaDigitalnihUlazaIzlaza();
    inicijalizacijaTimera();
    inicijalizacijaVanjskihPrekida();
    inicijalizacijaUSART();
    lcd_init(LCD_DISP_ON);
    sei(); // Globalno omogu�avanje prekida
}

int main(void) {
    inicijalizacija();
    ukupnaUdaljenost = eeprom_citaj_kilometrazu(); // �itanje kilometra�e iz EEPROM-a

    while(1) {
        // Provjera stanja tipkala za trubu
        if (!(PIND & (1 << PD0))) {
            BUZZ(0.5, 1000); // Tipkalo pritisnuto, zujalica se ogla�ava
        }

        // Provjera tipkala za kratka svjetla
        if (!(PIND & (1 << PD1))) {
            OCR1A = 128; // Kratka svjetla, srednja svjetlina
        } else {
            OCR1A = 0; // Isklju�ivanje kratkih svjetala
        }

        // Provjera tipkala za duga svjetla
        if (!(PINF & (1 << PF6))) {
            OCR1B = 255; // Duga svjetla, maksimalna svjetlina
        } else {
            OCR1B = 0; // Isklju�ivanje dugih svjetala
        }

        _delay_ms(1000); // �ekanje 1 sekundu prije ponovnog slanja podataka
        prikaziBrzinu();
        bluetooth();
    }
}

void inicijalizacijaDigitalnihUlazaIzlaza() {
    // Konfiguracija PD2 (INT0) kao ulazni za Hall senzor
    DDRD &= ~(1 << DDD2);
    PORTD |= (1 << PORTD2); // Aktiviranje pull-up otpornika

    // Konfiguracija pinova za LCD displej (primjer)
    DDRB |= (1 << DDB0) | (1 << DDB1) | (1 << DDB2) | (1 << DDB3);
    
    // Konfiguracija pinova za Bluetooth (USART)
    DDRD |= (1 << DDD1); // TX kao izlazni
    DDRD &= ~(1 << DDD0); // RX kao ulazni
    
    // Konfiguracija pinova za LED diode
    DDRB |= (1 << DDB4) | (1 << DDB5) | (1 << DDB6) | (1 << DDB7);
    PORTB &= ~((1 << PORTB4) | (1 << PORTB5) | (1 << PORTB6) | (1 << PORTB7)); // Isklju�ivanje svih dioda na po�etku

    // Konfiguracija pinova za buzzer
    BUZZER_DDR |= (1 << BUZZER_PIN);
    BUZZER_PORT &= ~(1 << BUZZER_PIN); // Isklju�ivanje zujalice na po�etku

    // Konfiguracija tipkala za trubu
    DDRD &= ~(1 << DDD0); // PD0 kao ulazni
    PORTD |= (1 << PORTD0); // Aktiviranje pull-up otpornika za tipkalo
    
    // Konfiguracija tipkala za kratka i duga svjetla
    DDRD &= ~(1 << DDD1); // PD1 kao ulazni za kratka svjetla
    PORTD |= (1 << PD1); // Aktiviranje pull-up otpornika za tipkalo

    DDRF &= ~(1 << DDF6); // PF6 kao ulazni za duga svjetla
    PORTF |= (1 << PF6); // Aktiviranje pull-up otpornika za tipkalo
    
    // PWM konfiguracija za LED diode (kratka i duga svjetla)
    DDRB |= (1 << DDB1) | (1 << DDB2); // Postavljanje PB1 i PB2 kao izlazne
    TCCR1A |= (1 << COM1A1) | (1 << COM1B1) | (1 << WGM10); // PWM, Phase Correct, 8-bit
    TCCR1B |= (1 << CS11); // Prescaler 8
}

void inicijalizacijaTimera() {
    // Postavljanje tajmera 1 u CTC mod
    TCCR1B |= (1 << WGM12);
    TIMSK1 |= (1 << OCIE1A); // Omogu�avanje prekida
    OCR1A = 15624; // Postavljanje OCR1A za 1 sekundu (s pretpostavljenim clockom 16MHz i prescalerom 1024)
    TCCR1B |= (1 << CS12) | (1 << CS10); // Postavljanje prescalera na 1024
}

// ISR za vanjski prekid
ISR(INT0_vect) {
    // Hall senzor �e generirati impuls svaki put kad detektira magnetsko polje. Moramo brojati ove impulse i izra�unati brzinu.
    brojacImpulsa++;
}

// ISR za tajmer
ISR(TIMER1_COMPA_vect) {
    float pre�enaUdaljenost = (brojacImpulsa * OPSEG_KOTACA) / BROJ_MAGNETA;
    ukupnaUdaljenost += pre�enaUdaljenost;
    brzina = pre�enaUdaljenost;
    brojacImpulsa = 0;
    prikaziBrzinu();
    bluetooth();
    updateLEDs();
    eeprom_pisi_kilometrazu(ukupnaUdaljenost); // Pohranjivanje kilometra�e
    if (brzina >= 10) {
        BUZZ(2, 1000); // Automatsko ogla�avanje zujalice na 2 sekunde
    }
}

void inicijalizacijaVanjskihPrekida() {
    EICRA |= (1 << ISC01); // Falling edge na INT0
    EIMSK |= (1 << INT0); // Omogu�avanje INT0 prekida
}

// Inicijalizacija USART
void inicijalizacijaUSART() { // Konfiguracija USART za Bluetooth komunikaciju i slanje podataka.
    UBRR1H = 0;
    UBRR1L = 103; // Baud rate 9600
    UCSR1B = (1 << TXEN1) | (1 << RXEN1); // Omogu�avanje prijenosa i prijema
    UCSR1C = (1 << UCSZ11) | (1 << UCSZ10); // 8-bit data
}

void usart_send_char(char c) {
    while (!(UCSR1A & (1 << UDRE1)));
    UDR1 = c;
}

void usart_send_string(char* str) {
    while (*str) {
        usart_send_char(*str++);
    }
}

void bluetooth() {
    char buffer[16];
    snprintf(buffer, sizeof(buffer), "Speed: %.2f m/s\n", brzina);
    usart_send_string(buffer);
}

void prikaziBrzinu() { // Mo�da se nece prikazivati brzina jer je krivi display, ovo je 16x2 LCD displej s HD44780 kontrolerom.
    char buffer[16];
    snprintf(buffer, sizeof(buffer), "Speed: %.2f m/s", brzina);
	// tadkoer dodaj da ce cita iz eeproma kilometraza, i ispisuje na lcd
    lcd_clear();
    lcd_print(buffer);
}

void updateLEDs() {
    // Paljenje dioda ovisno o brzini
    if (brzina < 2) {
        PORTB |= (1 << PORTB4); // Crvena dioda
        PORTB &= ~((1 << PORTB5) | (1 << PORTB6) | (1 << PORTB7)); // Ostale isklju�ene
    } else if (brzina >= 2 && brzina < 6) {
        PORTB |= (1 << PORTB4) | (1 << PORTB5); // Crvena i �uta dioda
        PORTB &= ~((1 << PORTB6) | (1 << PORTB7)); // Ostale isklju�ene
    } else if (brzina >= 6 && brzina < 10) {
        PORTB |= (1 << PORTB4) | (1 << PORTB5) | (1 << PORTB6); // Crvena, �uta i zelena dioda
        PORTB &= ~(1 << PORTB7); // Plava isklju�ena
    } else if (brzina >= 10) {
        PORTB |= (1 << PORTB4) | (1 << PORTB5) | (1 << PORTB6) | (1 << PORTB7); // Sve diode uklju�ene
    }
}

void eeprom_pisi_kilometrazu(float kilometraza) {
    eeprom_update_float((float*)0, kilometraza);
}

float eeprom_citaj_kilometrazu() {
    return eeprom_read_float((float*)0);
}
